## configuration file

For the example "Alice" user, `tuning` automatically reads (depending on the platform):

- Linux: /home/alice/.config/tuning/main.toml
- macOS: /Users/Alice/Library/Preferences/tuning/main.toml
- Windows: C:\Users\Alice\AppData\Roaming\tuning\main.toml

## jobs

All types of jobs can have the following optional fields:

```toml
[[job]]
name = ""
needs = [""]
```

### command

inspired by: https://docs.ansible.com/ansible/latest/modules/command_module.html#command-module

```toml
[[job]]
type = "command"  # required
argv = [""]
chdir = ""
command = ""      # required
creates = ""
env = { "" = "" }
removes = ""
```

### fake

```toml
[[job]]
type = "fake"   # required
sleep_ms = 1000
state = "changed"
```

### file

inspired by: https://docs.ansible.com/ansible/latest/modules/file_module.html#file-module

```toml
[[job]]
type = "file"  # required
force = false
src = ""
path = ""      # required
state = "link" # required
```

### git

inspired by: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html#ansible-collections-ansible-builtin-git-module

```toml
[[job]]
type = "git"  # required
clone = true
depth = 1
dest = ""     # required
force = false
repo = ""     # required
```

### installercargo

```toml
[[job]]
type = "installercargo" # required
crates = [ "" ]
state = "present"       # required
```

### installergo

```toml
[[job]]
type = "installergo"    # required
packages = [ "" ]
state = "latest"        # required
```

- `packages` strings may be crate names as they appear at [crates.io](https://crates.io)
- `packages` strings may be arbitrary git URLs that begin with `https://` and end with `.git`

### installernpm

```toml
[[job]]
type = "installernpm"   # required
packages = [ "" ]
state = "present"       # required
```
