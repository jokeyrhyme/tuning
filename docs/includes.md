# configuration file

## includes

Your main file may refer to other files to include:

```
[[includes]]
src = "./path/to/other/file.toml"
```

- `src` can be absolute, otherwise `tuning` interprets it as relative to the file that contains the `[[includes]]`

- all configuration files are recursively-crawled before any template evaluation and before running any jobs

- a src with a path to a missing file is an error

- you may include more than one reference to the same file,
  and `tuning` will read that file once
  (to allow e.g. files A and B to both express their dependency on file C)

- we do not define nor guarantee the expected sequence of jobs after including some from another file
  (if job sequence is important, you must explicitly use [`needs`](./jobs.md))
