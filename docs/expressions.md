# expressions

when defining the values of string fields for a [job](./jobs.md),
you may provide a string value containing a [tera expression](https://tera.netlify.app/docs) in any field value,
e.g. `"{{ 'foo' | upper }}"`

you may provide such an expression string value for boolean fields,
as long as the expression results in a boolean value,
or the string values `"true"` or `"false"`,
e.g. `when = "{{ 1 is odd }}"` (TODO: check this example)

you may provide such an expression string value for numeric fields,
as long as the expression results in a numeric value,
or a string that `tuning` can parse as a numeric value
e.g. `depth = "{{ 2 + 2 }}"` (TODO: check this example)

you may provide such an expression string value within arrays and tables,
as long as the expression results in the expected kind of nested value,
or a string that `tuning` can parse as the expected kind

the "name", "needs", and "type" fields ignore expressions, so you should not use expressions here

`tuning` evaluates expressions within the definition of a job:

- after its `"needs"` are successful; and
- right before attempting to run it

you must define all expressions within strings,
anywhere else and your config file is not valid TOML

## facts

the following `tuning`-specific values are available,
for use within expressions

see the [`Facts`](../src/lib/facts.rs) struct for low-level details

### cache_dir (path)

as defined over in the [dirs crate](https://crates.io/crates/dirs)

e.g. ~/.cache (Linux)

### config_dir (path)

as defined over in the [dirs crate](https://crates.io/crates/dirs)

e.g. ~/.config (Linux)

### home_dir (path)

as defined over in the [dirs crate](https://crates.io/crates/dirs)

e.g. ~/ (Linux)

### is_distro_archlinux (boolean)

`true` if distribution is archlinux

### is_distro_armbian (boolean)

`true` if distribution is armbian

### is_distro_raspbian (boolean)

`true` if distribution is raspbian

### is_os_linux (boolean)

`true` if OS is Linux

### is_os_macos (boolean)

`true` if OS is macOS

### is_os_windows (boolean)

`true` if OS is Windows

### main_file (path)

path to the main.toml file used for this run

e.g. ~/.config/tuning/main.toml

## functions

### has_executable (exe:string -> boolean)

`true` if a given executable is available (i.e. in the PATH)

e.g. `{{ has_executable(exe="tuning") }}`
