## concurrency

- by default, `tuning` runs jobs concurrently
- to serialise jobs in relation to each other, you can use the `name` and `needs` fields
- jobs run as a graph of [`Future`](https://doc.rust-lang.org/stable/std/future/trait.Future.html)s,
  using [tokio](https://tokio.rs/)'s multi-threaded runtime
- each job awaits the outcome of all the jobs that it `needs`

## serial jobs

- we've already encountered some jobs that are arguably better if no other jobs are running concurrently, e.g:

  - "command" jobs that take over the whole interactive terminal session
  - jobs that need to make requests to rate-limited remote services

- ideally these cases could be auto-detected or even hard-coded, but we'll probably end up offering a job field to explicitly request serial execution

  - it wouldn't be wise to overload the `needs` field, as serial jobs may still have dependencies
