# job metadata

these are fields that are not specific to the job type,
but rather relate to running the job

## name (string, optional)

set a unique name / label / description / identifier for the job,
which appears in logs when the job runs

e.g.

```
[[jobs]]
name = "something to do"
# ...
```

## needs (string[], optional)

set dependencies for the job,
which **all** need to complete without errors,
before this job can run

e.g.

```
[[jobs]]
name = "first thing"
# ...

[[jobs]]
name = "second thing"
# ...
needs = ["first thing"]
```

## tags (string[], optional)

define whether the job will run,
depending on the following arguments for `tuning`:

- `--tags all`: job will run, its "tags" do not matter (default)
- `--tags cat,dog`: job will if its "tags" contains at least one of `"cat"` or `"dog"`,
- `--skip-tags cat,dog`: job will not run if its "tags" contains either `"cat"` or `"dog"`
- `--tags tagged`: job will run if its "tags" has at least one value
- `--tags untagged`: job will run if its "tags" is empty

### special tag: `"always"`

job will run even when `--tags` does not match any of its "tags"

job will not run if `--skip-tags always`

### special tag: `"never"`

job will not run even when `--skip-tags` does not match any of its "tags"

job will run during if `--tags never`

## when (boolean; default = true)

e.g.

```
[[jobs]]
name = "something to do"
# ...
when = true
```

- `true`: run the job
- `false`: skip the job

this makes the most sense when combined with a boolean
[template expression](./template.md)

e.g.

```
[[jobs]]
name = "something to do"
# ...
when = {{ is_os_linux or is_os_macos }}
```
