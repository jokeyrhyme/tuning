# tuning [![Status](https://img.shields.io/badge/status-actively--developed-brightgreen)](https://gitlab.com/jokeyrhyme/tuning) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/tuning?branch=main)](https://gitlab.com/jokeyrhyme/tuning/-/pipelines?ref=main) [![Crates.io](https://img.shields.io/crates/v/tuning)](https://crates.io/crates/tuning) [![docs.rs](https://img.shields.io/docsrs/tuning)](https://docs.rs/tuning)

ansible-like tool with a smaller scope, focused primarily on complementing dotfiles for cross-machine bliss

# status

- some functionality,
  but still missing some basics that will make it actually useful

# what?

- inspired by [`ansible`](https://www.ansible.com/),
  with jobs defined in a declarative file format

- will focus on the dotfiles use-case:
  maintaining the same user preferences across machines,
  or restoring user preferences to a fresh machine

- no remote or fleet functionality / complexity

- not aiming to do anything that requires root / Administrator permissions (yet)

- named after the reality-bending ability in [Dark City](https://en.wikipedia.org/wiki/Dark_City_%281998_film%29)

# why?

- my dotfiles started as a [whole bunch of shell-scripts](https://github.com/jokeyrhyme/dotfiles),
  but didn't have any support for Windows,

- I'd partially moved to [my second attempt](https://github.com/jokeyrhyme/dotfiles-rs),
  which is cross-platform,
  but required too much work for new jobs

- other existing tools use interpretted languages,
  which are fine for web services that run in containers,
  but can be overly-sensitive to interpreter versions and globally-installed packages

- yes, I am firmly trapped in [The Code/Data Cycle](https://twitter.com/niklasfrykholm/status/1063242674717679621)

# prerequisites

- Rust compiler and `cargo`: https://rustup.rs/

# getting started

```
$ cargo install tuning
$ tuning
```

# documentation

- read about [contributing to this project](./CONTRIBUTING.md)
- read about [job metadata](./docs/metadata.md)
- read about [job file template rendering](./docs/template.md)
- read about the [jobs configuration file format](https://github.com/jokeyrhyme/tuning/wiki/Jobs-definition)

# roadmap for 1.0

- [ ] support machine hostname as a `when` condition ([#10](https://github.com/jokeyrhyme/tuning/issues/10))
- [x] `--config` flag to point at a different config file ([#11](https://github.com/jokeyrhyme/tuning/issues/11))
- [ ] support job status as a `when` condition ([#12](https://github.com/jokeyrhyme/tuning/issues/12))
- [x] `[[includes]]` to help decompose large config files ([#15](https://github.com/jokeyrhyme/tuning/issues/15))
- [ ] validate that there are no circular dependencies between jobs ([#17](https://gitlab.com/jokeyrhyme/tuning/-/issues/17))

# known issues

## security

- this project is vulnerable to [symlink race](https://en.wikipedia.org/wiki/Symlink_race)s,
  which is why we do not recommend running it with root privileges for now

# see also

- https://github.com/SuperCuber/dotter
- https://github.com/ema/pets
- https://github.com/oknozor/toml-bombadil
- https://github.com/rash-sh/rash
- https://github.com/twpayne/chezmoi
- https://www.ansible.com/
