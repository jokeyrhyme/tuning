# jokeyrhyme / tuning

## contributing

- this repository follows [Conventional Commits 1.0.0](https://www.conventionalcommits.org/en/v1.0.0/)
- this repository follows [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0/)
- this repository follows [SemVer 2.0.0](https://semver.org/spec/v2.0.0.html)
