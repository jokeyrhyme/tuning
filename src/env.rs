use std::env;

use camino::Utf8PathBuf;

pub(crate) fn current_dir() -> Utf8PathBuf {
    // panic: let's bail early if we somehow get into this state
    let cwd = env::current_dir().expect("cannot determine current working directory");
    Utf8PathBuf::from_path_buf(cwd).expect("current working directory has invalid characters")
}
