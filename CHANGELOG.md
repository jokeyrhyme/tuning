# Changelog

We will document all notable changes to this project in this file.

We use the [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format,
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- display job status when `Blocked` and `Skipped`

## [0.5.0] - 2024-01-24

### Added

- "tags" field for jobs
- `--tags all` (default)
- `--tags <TAGS>` to run jobs with at least one matching value in "tags"
- `--tags never` to run jobs with "tags" field containing "never"
- `--tags tagged` to run jobs with a non-empty list of "tags"
- `--tags untagged` to run jobs with an empty list of "tags"
- `--skip-tags always` to skip jobs with "tags" field containing "always"
- `--skip-tags <SKIP_TAGS>` to jobs with at least one matching value in "tags"

### Changed

- removed the `-c` short command line argument for `--config`

- removed the non-specific "done" status, ensuring all successful jobs return "changed" or "nochange"

  - this will break any "fake" jobs that specify the "done" status, please switch to "changed"

## [0.4.2] - 2023-09-30

### Fixed

- addressed some `cargo clippy` warnings

## [0.4.1] - 2023-09-30

### Changed

- installercargo: install using [`--locked`](https://doc.rust-lang.org/cargo/commands/cargo-install.html#dealing-with-the-lockfile)

## [0.4.0] - 2022-11-20

### Added

- new `[[includes]]` directive

- new `--config`

## [0.3.0] - 2022-10-08

### Changed

- overhaul job runner to use Futures ([#1](https://gitlab.com/jokeyrhyme/tuning/-/issues/1))

  - less waiting/locking/bookkeeping between jobs
  - slightly higher memory usage as we now have a Futures runtime and more cloning of values

### Fixed

- removed some more `dbg!`, oops!

## [0.2.1] - 2022-09-10

### Added

- installercargo: support unpublished git crates ([#16](https://gitlab.com/jokeyrhyme/tuning/-/issues/16))

## [0.2.0] - 2022-08-21

### Added

- as a fallback, any job that has no name gets a random UUID instead

### Changed

- `tuning` now separately evaluates expressions per-job, instead of per-config-file
- `tuning` now evaluates expressions for a job after its "needs" (if any) have finished
- `tuning` now evaluates expressions for a job right before running it (this includes expressions within the "when" field)

### Fixed

- git: no longer prints the git version with each job run

### Removed

- expressions (e.g. `{{ ... }}`) in your config file are now not valid unless they are in strings

## [0.1.21] - 2022-08-13

### Changed

- concurrency in runner based on number of CPUs

### Fixed

- installercargo, installernpm: correct status messages for `"latest"`

## [0.1.20] - 2021-11-07

### Added

- installernpm: new job type to (un)install npm packages

### Fixed

- remove some `dbg!`s, oops!

## [0.1.19] - 2021-11-07

### Added

- installercargo: new job type to (un)install Rust crates

## [0.1.18] - 2021-10-04

### Added

- new `is_distro_...` facts

## [0.1.17] - 2021-09-11

### Added

- command: new `env` field for controlling environment variables
- installergo: new job type to (un)install Go packages

## [0.1.16] - 2021-07-15

### Changed

- git: add fallback logic when `force` and `update` have initial error

## [0.1.15] - 2021-06-27

### Added

- git: new `update` field to specify whether to update an existing repository

## [0.1.14] - 2021-06-14

### Changed

- report job summary at the end, not during

### Fixed

## [0.1.13] - 2021-06-14

### Added

- git: [new job type](./docs/jobs.md) for version control operations

## [0.1.12] - 2021-04-10

### Added

- facts: new "main_file" fact for path to current main.toml

### Fixed

- constrain template search to directory of main.toml file (fixes #4)

## [0.1.11] - 2021-04-09

### Added

- output a summary of job statuses every 2 seconds

### Fixed

- fake: read sleep_ms integer as a `std::time::Duration`

## [0.1.10] - 2021-04-08

### Added

- fake: [new job type](./docs/jobs.md) for debugging and testing

### Changed

- shuffle Status varations around so that `Blocked` means blocked forever

## [0.1.9] - 2020-08-08

### Changed

- no longer output log message when starting a job

## [0.1.8] - 2020-05-03

### Added

- `"when"` for conditional jobs
- new [facts](./docs/template.md) for OS-detection
- document available template expression values / [facts](./docs/template.md)
- `{{ has_executable(foo) }}` template function to check for executables

### Changed

- also check for tuning/main.toml in ~/.dotfiles

### Fixed

- escape path expressions so Windows paths are valid TOML (#17)

## [0.1.7] - 2020-04-06

### Added

- colorized output for job status
- generate friendlier names for command jobs
- generate friendlier names for file jobs
- internally centralise handling of common fields like `"name"`

## [0.1.6] - 2020-03-13

### Added

- links to wiki documentation in README
- read config file from default location
- `"needs"` for inter-job dependencies
- command: job type to run commands
- concurrent job runner using 2 threads
- file: job type to manipulate files
- support `{{ home_dir }}` and other expressions in template

## [0.1.5] - 2019-08-16

### Fixed

- ci: cannot use `contains()` with array

## [0.1.4] - 2019-08-16

### Fixed

- ci: tweak handling of "release" GitHub Action

## [0.1.3] - 2019-08-16

### Fixed

- add missing description and license metadata

## [0.1.2] - 2019-08-16

### Added

- ci: debug GitHub Actions workflow

## [0.1.1] - 2019-08-16

### Fixed

- ci: fix broken GitHub Actions `if`

## [0.1.0] - 2019-08-16

### Added

- initial (non-functional) release to crates.io
